// main.cpp


#include <sqlite3.h>
#include <iostream>
#include <jap_self_study/jap_self_study_core.h>



int main()
{
    jap_self_study::JapStudyCore core;
    std::cout << core.kanjis.size() << std::endl;
    for (auto i = core.kanjis.begin(); i != core.kanjis.end(); i++)
        std::cout << (*i)->character << " ";
    std::cout << std::endl;

    /*std::cout << "Words:" << std::endl;
    for (auto w = core.words.begin(); w != core.words.end(); w++)
    {
        for (auto e = (*w)->elements.begin(); e != (*w)->elements.end(); e++)
            std::cout << (*e)->reading;
        std::cout << std::endl;
    }*/
}
