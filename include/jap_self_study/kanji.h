// kanji.h



#ifndef KANJI
#define KANJI



#include <deque>
#include "kanji_reading.h"



namespace jap_self_study {



class KanjiReading;



class Kanji
{
public:
    std::string character;
    std::deque<KanjiReading*> readings;
    unsigned char level;
};



}



#endif // KANJI
