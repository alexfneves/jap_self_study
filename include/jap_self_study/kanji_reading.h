// kanji_reading.h



#ifndef KANJI_READING
#define KANJI_READING



#include "element.h"
#include "kanji.h"
#include "word.h"



namespace jap_self_study {



class Kanji;



enum ReadingType
{
    ONYOMI,
    KUNYONI,
    SPECIALYOMI
};


class KanjiReading : public Element
{
public:
    Kanji* kanji;
    ReadingType type;

    virtual ~KanjiReading(){}
};



}



#endif // KANJI_READING
