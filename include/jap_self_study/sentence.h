// sentence.h



#ifndef SENTENCE
#define SENTENCE



#include <deque>
#include "word.h"



namespace jap_self_study {



class Sentence
{
public:
    std::deque<Word*> words;
};



}



#endif // SENTENCE
