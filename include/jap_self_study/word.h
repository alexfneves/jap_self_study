// word.h



#ifndef WORD
#define WORD



#include <string>
#include <deque>
#include "element.h"



namespace jap_self_study {



class Word
{
public:
    std::string meaning;
    std::deque<Element*> elements;
    unsigned char level;
};



class Substantive : public Word
{
public:
};



class Adjective : public Word
{
public:
};



class IAdjective : public Adjective
{
public:
};



class NaAdjective : public Adjective
{
public:
};



class Verb : public Word
{
public:
    unsigned char group = 0;
};



}



#endif // WORD
