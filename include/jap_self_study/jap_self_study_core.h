// jap_self_study_core.h



#ifndef JAP_SELF_STUDY_CORE
#define JAP_SELF_STUDY_CORE



#include <map>
#include "kanji.h"
#include "word.h"



namespace jap_self_study {



class JapStudyCore
{
    std::map<std::string, std::string> kataToHira;

    std::deque<std::string> SeparateString(std::string s, char c);

    bool IsHiraKata(std::string str);

    std::string ToHiragana(std::string str);

    Word* FindWordWithKanjis(std::deque<Kanji*> kanjiList, const std::string & word, const std::string & wordInKanji, std::string formedWord = "", unsigned int index = 0);

    void LoadKanjiList(std::string kanjisFile, unsigned char level);
    void LoadVocabList(std::string vocabFile, unsigned char level);

    void Save();
    void Load();

public:
    std::deque<Kanji*> kanjis;
    std::deque<Word*> words;

    JapStudyCore();
    virtual ~JapStudyCore();
};



}



#endif // JAP_SELF_STUDY_CORE
