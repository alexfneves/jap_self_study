// jap_self_study_core.cpp



#include <iostream>
#include <fstream>
#include <jap_self_study/jap_self_study_core.h>



jap_self_study::JapStudyCore::JapStudyCore()
{
    kataToHira = {
        {"ア", "あ"}, {"イ", "い"}, {"ウ", "う"}, {"エ", "え"}, {"オ", "お"},
        {"カ", "か"}, {"キ", "き"}, {"ク", "く"}, {"ケ", "け"}, {"コ", "こ"},
        {"ガ", "が"}, {"ギ", "ぎ"}, {"グ", "ぐ"}, {"ゲ", "げ"}, {"ゴ", "ご"},
        {"サ", "さ"}, {"シ", "し"}, {"ス", "す"}, {"セ", "せ"}, {"ソ", "そ"},
        {"ザ", "ざ"}, {"ジ", "じ"}, {"ズ", "ず"}, {"ゼ", "ぜ"}, {"ゾ", "ぞ"},
        {"タ", "た"}, {"チ", "ち"}, {"ツ", "つ"}, {"テ", "て"}, {"ト", "と"},
        {"ダ", "だ"}, {"ヂ", "ぢ"}, {"ヅ", "づ"}, {"デ", "で"}, {"ド", "ど"},
                                  {"ッ", "っ"},
        {"ナ", "な"}, {"ニ", "に"}, {"ヌ", "ぬ"}, {"ネ", "ね"}, {"ノ", "の"},
        {"ハ", "は"}, {"ヒ", "ひ"}, {"フ", "ふ"}, {"ヘ", "へ"}, {"ホ", "ほ"},
        {"バ", "ば"}, {"ビ", "び"}, {"ブ", "ぶ"}, {"ベ", "べ"}, {"ボ", "ぼ"},
        {"パ", "ぱ"}, {"ピ", "ぴ"}, {"プ", "ぷ"}, {"ペ", "ぺ"}, {"ポ", "ぽ"},
        {"マ", "ま"}, {"ミ", "み"}, {"ム", "む"}, {"メ", "め"}, {"モ", "も"},
        {"ヤ", "や"},              {"ユ", "ゆ"},              {"ヨ", "よ"},
        {"ャ", "ゃ"},              {"ュ", "ゅ"},              {"ョ", "ょ"},
        {"ラ", "ら"}, {"リ", "り"}, {"ル", "る"}, {"レ", "れ"}, {"ロ", "ろ"},
        {"ワ", "わ"},                                    {"ヲ", "を"},
        {"ン", "ん"}
    };

    Load();
}


std::deque<std::string> jap_self_study::JapStudyCore::SeparateString(std::string s, char c)
{
    std::deque<std::string> ret;

    int n1 = s.find(c, 0);
    int n2 = 0;
    if (n1 >= 0)
    {
        ret.push_back(s.substr(n2, n1 - n2));

        while (n1 >= 0)
        {
            n2 = n1 + 1;
            n1 = s.find(c, n1 + 1);
            if (n1 >= 0)
                ret.push_back(s.substr(n2, n1 - n2));
            else if (s.substr(n2, s.size() - n2) != "")
                ret.push_back(s.substr(n2, s.size() - n2));
        }
        return ret;
    }
    else
    {
        if (s != "")
            ret.push_back(s);
        return ret;
    }
}


bool jap_self_study::JapStudyCore::IsHiraKata(std::string str)
{
    for (auto j = kataToHira.begin(); j != kataToHira.end(); j++)
    {
        if (str == j->first || str == j->second)
            return true;
    }
    return false;
}


std::string jap_self_study::JapStudyCore::ToHiragana(std::string str)
{
    std::string ret;

    for (auto i = 0; i < str.size();)
    {
        if (str[i] == '.' || str[i] == '-')
             i++;
        else
        {
            std::string temp = str.substr(i, 3);
            /*auto found = kataToHira.find(temp);
            if (found != kataToHira.end())
                ret += found->second;*/
            for (auto j = kataToHira.begin(); j != kataToHira.end(); j++)
            {
                if (temp == j->first || temp == j->second)
                    ret += j->second;
            }
            i += 3;
        }
    }

    //std::cout << str << " to " << ret << std::endl;
    return ret;
}


jap_self_study::Word* jap_self_study::JapStudyCore::FindWordWithKanjis(std::deque<Kanji*> kanjiList, const std::string & word, const std::string & wordInKanji, std::string formedWord, unsigned int index)
{
    //if (word == "ぬるい")
        //std::cout << std::endl;

    if (index > wordInKanji.size())
        return NULL;

    if (word == formedWord)
        return new Word;

    std::string possibleHiraKata = wordInKanji.substr(index, 3);
    if (IsHiraKata(possibleHiraKata))
    {
        std::string newFormedWord = formedWord + possibleHiraKata;
        auto newWord = FindWordWithKanjis(kanjiList, word, wordInKanji, newFormedWord, index + 3);
        if (newWord != NULL)
        {
            Element* hiraKata = new Element;
            hiraKata->reading = possibleHiraKata;
            newWord->elements.push_front(hiraKata);
            return newWord;
        }
    }
    else if (kanjiList.size() != 0)
    {
        /*if (kanjiList.size() > 1)
        {
            for (int i = 0; i < kanjiList.size(); i++)
                for (int j = 0; j != kanjiList[i]->readings.size(); j++)
                    std::cout << kanjiList[i]->character << ": "<< kanjiList[i]->readings[j]->reading << std::endl;
            std::cout << word << std::endl;
        }*/

        Kanji* currentKanji = kanjiList.front();
        kanjiList.pop_front();
        for (auto i = 0; i < currentKanji->readings.size(); i++)
        {
            std::string newFormedWord = formedWord + ToHiragana(currentKanji->readings[i]->reading);
            //std::cout << newFormedWord << std::endl;
            auto newWord = FindWordWithKanjis(kanjiList, word, wordInKanji, newFormedWord, index + currentKanji->character.size());
            if (newWord != NULL)
            {
                newWord->elements.push_front(currentKanji->readings[i]);
                return newWord;
            }
        }

        return NULL;
    }

    return NULL;
}


void jap_self_study::JapStudyCore::LoadKanjiList(std::string kanjisFile, unsigned char level)
{
    std::ifstream file(kanjisFile);
    std::string line;
    if (file.is_open())
    {
        std::getline(file, line); // discard first line
        while (std::getline(file, line))
        {
            auto sections = SeparateString(line, '\t');
            if (sections.size() >= 4)
            {
                auto kanji = new Kanji;
                kanji->character = sections[0];
                kanji->level = level;
                this->kanjis.push_back(kanji);

                std::deque<std::string> readings = SeparateString(sections[1], ' ');
                std::deque<std::string> readings2 = SeparateString(sections[2], ' ');
                std::deque<std::string> readings3 = SeparateString(sections[3], ' ');
                for (auto i = readings2.begin(); i != readings2.end(); i++)
                    readings.push_back(*i);
                for (auto i = readings3.begin(); i != readings3.end(); i++)
                    readings.push_back(*i);

                // this code was separating readings contaning "." into two readings
                // now I think the "." separates the reading part that stay inside the kanji from the part that stays outside the string
                /*for (auto i = 0; i != readings.size(); i++)
                {
                    std::cout << readings[i] << std::endl;
                    auto n = readings[i].find('.');
                    if (n != -1)
                    {
                        auto temp = readings[i].substr(n + 1, readings[i].size());
                        readings[i] = readings[i].substr(0, n);
                        readings.insert(readings.begin() + i, temp);
                    }
                }*/

                for (auto i = 0; i != readings.size();)
                {
                    if (readings[i] == "-")
                    {
                        readings.erase(readings.begin() + i);
                        continue;
                    }
                    i++;
                }

                for (auto r = readings.begin(); r != readings.end();r++)
                {
                    bool ok = true;
                    for (auto kReading = kanji->readings.begin(); kReading != kanji->readings.end(); kReading++)
                    {
                        if ((*kReading)->reading == *r)
                        {
                            ok = false;
                            break;
                        }
                    }

                    if (ok)
                    {
                        auto reading = new KanjiReading;
                        reading->reading = *r;
                        kanji->readings.push_back(reading);
                        reading->kanji = kanji;
                    }
                }
            }
        }
        file.close();
    }
}


void jap_self_study::JapStudyCore::LoadVocabList(std::string vocabFile, unsigned char level)
{
    std::deque<std::string> errorWords;

    std::ifstream file(vocabFile);
    std::string line;
    if (file.is_open())
    {
        std::getline(file, line); // discard first line
        while (std::getline(file, line))
        {
            auto sections = SeparateString(line, '\t');
            if (sections.size() >= 3)
            {
                if (sections[0] == "")
                {
                    Element* element = new Element;
                    element->reading = sections[1];
                    Word* word = new Word;
                    word->meaning = sections[2];
                    word->level = level;
                    words.push_back(word);
                    //std::cout << "OK " << sections[1] << std::endl;
                }
                else
                {
                    //std::cout << "word: " << sections[0] << std::endl;

                    std::deque<Kanji*> kanjisFound;
                    std::deque<size_t> positionOfKanjisFound;

                    for (auto i = kanjis.begin(); i != kanjis.end(); i++)
                    {
                        size_t p = 0;
                        while (true)
                        {
                            auto found = sections[0].find((*i)->character, p);
                            if (found == std::string::npos)
                                break;
                            else
                            {
                                p = found + 1;
                                if (positionOfKanjisFound.size() == 0)
                                {
                                    kanjisFound.push_back(*i);
                                    positionOfKanjisFound.push_back(found);
                                }
                                else
                                {
                                    bool inserted = false;
                                    for (auto j = positionOfKanjisFound.begin(); j != positionOfKanjisFound.end(); j++)
                                    {
                                        if (*j > found)
                                        {
                                            kanjisFound.insert(kanjisFound.begin() + (j - positionOfKanjisFound.begin()), *i);
                                            positionOfKanjisFound.insert(j, found);
                                            inserted = true;
                                            break;
                                        }
                                    }
                                    if (!inserted)
                                    {
                                        kanjisFound.push_back(*i);
                                        positionOfKanjisFound.push_back(found);
                                    }
                                }
                            }
                        }
                    }

                    Word* word = FindWordWithKanjis(kanjisFound, sections[1], sections[0]);
                    if (word == NULL)
                    {
                        errorWords.push_back(sections[1]);
                        std::cout << "Error word: " << sections[1] << std::endl;
                        for (auto i = kanjisFound.begin(); i != kanjisFound.end(); i++)
                        {
                            std::cout << (*i)->character << " " << (int)(*i)->level;
                            for (auto j = (*i)->readings.begin(); j != (*i)->readings.end(); j++)
                                std::cout << " " << (*j)->reading;
                            std::cout << std::endl;
                        }
                    }
                    else
                    {
                        word->meaning = sections[2];
                        word->level = level;
                        words.push_back(word);
                        //std::cout << "OK " << sections[1] << std::endl;
                    }
                }
            }
        }
        file.close();
    }
}


void jap_self_study::JapStudyCore::Save()
{
}


void jap_self_study::JapStudyCore::Load()
{
    /*auto element1 = new Element;
    auto element2 = new Element;
    auto element3 = new Element;
    auto kanji1 = new Kanji;
    auto kanji2 = new Kanji;
    auto kanjiReading1 = new KanjiReading;
    auto kanjiReading2 = new KanjiReading;
    auto kanjiReading3 = new KanjiReading;
    auto word = new Word;

    element1->reading = '/';
    element2->reading = '*';
    element3->reading = '-';
    kanji1->character = 'a';
    kanji1->readings.push_back(kanjiReading1);
    kanji1->readings.push_back(kanjiReading2);
    kanji2->character = 'b';
    kanji2->readings.push_back(kanjiReading3);
    kanjiReading1->reading = '1';
    kanjiReading2->reading = '2';
    kanjiReading3->reading = '3';

    word->meaning = L"?????";
    word->elements.push_back(element1);
    word->elements.push_back(kanjiReading1);
    word->elements.push_back(element2);
    word->elements.push_back(kanjiReading2);
    word->elements.push_back(element3);

    kanjis.push_back(kanji1);
    kanjis.push_back(kanji2);
    words.push_back(word);*/


    LoadKanjiList("../resource/KanjiListN5", 5);
    LoadKanjiList("../resource/KanjiListN4", 4);
    LoadKanjiList("../resource/KanjiListN3", 3);
    LoadKanjiList("../resource/KanjiListN2", 2);
    LoadKanjiList("../resource/KanjiListN1", 1);

    LoadVocabList("../resource/VocabListN5", 5);
    LoadVocabList("../resource/VocabListN4", 4);
    LoadVocabList("../resource/VocabListN3", 3);
    LoadVocabList("../resource/VocabListN2", 2);
    LoadVocabList("../resource/VocabListN1", 1);
}


jap_self_study::JapStudyCore::~JapStudyCore()
{
    Save();
}
